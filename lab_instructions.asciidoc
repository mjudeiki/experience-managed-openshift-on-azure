// begin header
ifdef::env-gitlab[]
:tip-caption: :bulb:
:note-caption: :information_source:
:important-caption: :heavy_exclamation_mark:
endif::[]
:numbered:
:toc: macro
:toc-title: pass:[<b>Table of Contents</b>]
// end header

= OpenShift on Azure Lab Instructions

toc::[]

== Cluster Installation

In this exercise the participants will be able to deploy an OpenShift managed
cluster on Azure using Azure native client tools.

Tools used in this lab:

- oc - OpenShift client tools.
- az - Azure command line client tools.
- browser - To allow us to view resources inside of Microsoft Azure

.  Before we can install a cluster we need to login to Azure.  This can
 be accomplished by running the following command.
+
```
$ az login
```
+
.  In order to install a cluster we will need to set a few environment
variables:
+
- CLUSTER_NAME - A unique name for the cluster
+
```
$ CLUSTER_NAME=myuniqueclustername
```
+
- LOCATION - This determines the region for which to install the cluster
+
```
$ LOCATION=eastus
```
+
- FQDN - This will be used for the cluster's console.
+
```
$ FQDN=$CLUSTER_NAME.$LOCATION.cloudapp.azure.com
```
+
.  Create a resource group in which to house the OpenShift managed cluster
object.
+
```
az group create --name $CLUSTER_NAME --location $LOCATION
```
+
.  Install the OpenShift managed cluster.
+
```
az openshift create \
  --resource-group $CLUSTER_NAME \
  --name $CLUSTER_NAME \
  -l $LOCATION \
  --fqdn $FQDN
  --c 1
```

== Azure Active Directory Integration

Azure Active Directory is Microsoft's cloud-based identity and access
management service. This service will allow us to integrate our Azure users and
groups with our OpenShift cluster users and responsibilities.

TODO:

- Pre-setup AAD for users

OR

- Have users setup a group

== Deploy Open Service Broker for Azure

Open Service Broker for Azure is the open source, Open Service
Broker-compatible API server that provisions managed services in Microsoft
Azure public cloud.

The following steps will setup the open service broker azure:

. Setup the environment variables required to communicate with Azure.
+
```
$ AZURE_CLIENT_ID=
$ AZURE_CLIENT_SECRET=
$ AZURE_TENANT_ID=
$ AZURE_SUBSCRIPTION_ID=
```
+
. Create a project to where the osba pods will run.
+
```
$ oc new-project osba
```
+
. Create the osba pods.
+
```
$ oc process -f osba_template.yaml \
  -p ENVIRONMENT=AzurePublicCloud \
  -p AZURE_SUBSCRIPTION_ID=$AZURE_SUBSCRIPTION_ID \
  -p AZURE_CLIENT_ID=$AZURE_CLIENT_ID \
  -p AZURE_CLIENT_SECRET=$AZURE_CLIENT_SECRET \
  -p AZURE_TENANT_ID=$AZURE_TENANT_ID \
 | oc create -f -
```
+
. Verify the status of the osba pods:
+
```
$ oc get pods
NAME                                              READY     STATUS    RESTARTS   AGE
osba-open-service-broker-azure-7f9666f44b-7n8cm   1/1       Running   3          57m
osba-redis-6d474dfd99-nn2zm                       1/1       Running   0          57m
```


== Deploy a postgres database from Azure

Now that osba has been deployed, the instructions will guide us on how to
deploy a postgres database from with Azure.

. Create a project which will house our postgres backed application.
+
```
$ oc new-project app
```
+
. Create the postgres database
+
```
$ oc create postgres.yaml
$ oc create  -f postgres.yaml
serviceinstance.servicecatalog.k8s.io "example-postgresql-all-in-one-instance" created
$ oc get serviceinstance
NAME                                     AGE
example-postgresql-all-in-one-instance   16s
```
+
. Now create the binding object that maps to the service instace to the
postgres db
+
```
$ oc create -f binding.yaml
servicebinding.servicecatalog.k8s.io "example-postgresql-all-in-one-binding" created
$ oc get servicebinding
NAME                                    AGE
example-postgresql-all-in-one-binding   56s
$ oc get events
LAST SEEN   FIRST SEEN   COUNT     NAME                                                      KIND              SUBOBJECT   TYPE      REASON                  SOURCE                               MESSAGE
22s         1m           16        example-postgresql-all-in-one-binding.158af639d6bc04ce    ServiceBinding                Warning   ErrorInstanceNotReady   service-catalog-controller-manager   Binding cannot begin because referenced ServiceInstance "app/example-postgresql-all-in-one-instance" is not ready
3m          3m           1         example-postgresql-all-in-one-instance.158af6233f67e7ba   ServiceInstance               Normal    Provisioning            service-catalog-controller-manager   The instance is being provisioned asynchronously
```
+
This process usually takes around 5-7 minutes to register the postgresql
database inside of Azure.

```
$ oc describe serviceinstance
...
Status:
  Async Op In Progress:  false
  Conditions:
    Last Transition Time:  2019-03-11T17:09:23Z
    Message:               The instance was provisioned successfully
    Reason:                ProvisionedSuccessfully
    Status:                True
    Type:                  Ready
  Deprovision Status:      Required
...
Events:
  Type    Reason                   Age   From                                Message
  ----    ------                   ----  ----                                -------
  Normal  Provisioning             11m   service-catalog-controller-manager  The instance is being provisioned asynchronously
  Normal  ProvisionedSuccessfully  3m    service-catalog-controller-manager  The instance was provisioned successfully
```

== Deploy an application using postgres

A postgresql database has been created and awaiting a frontend.  This section
will configure an application that uses the previously created postgres db.

. Retrieve the database metadata including login, password, and connection
information
+
```
$ oc get secrets example-postgresql-all-in-one-secret -n app  -o yaml | python -c 'import yaml, sys, base64; z=yaml.load(sys.stdin.read());  map(lambda x: sys.stdout.write("export DATABASE_{}={}\n".format(x[0].upper(), base64.b64decode(x[1]))), z["data"].items())'

export DATABASE_USERNAME=x6d5a1fau7@46431014-d579-43d6-a570-f7411ed0fed8
export DATABASE_TAGS=["postgresql"]
export DATABASE_DATABASE=kpmu1itbso
export DATABASE_URI=postgresql://x6d5a1fau7%4046431014-d579-43d6-a570-f7411ed0fed8:rtcgoVpn3U35IC03@46431014-d579-43d6-a570-f7411ed0fed8.postgres.database.azure.com:5432/kpmu1itbso
export DATABASE_HOST=46431014-d579-43d6-a570-f7411ed0fed8.postgres.database.azure.com
export DATABASE_PASSWORD=rtcgoVpn3U35IC03
export DATABASE_PORT=5432
export DATABASE_SSLREQUIRED=false
```
+
. Copy and paste output to set environment variables
. Create an application
+
```

oc process -f django.json \
  -p DATABASE_SERVICE_NAME=postgres \
  -p DATABASE_NAME=$DATABASE_DATABASE \
  -p DATABASE_USER=$DATABASE_USERNAME \
  -p DATABASE_PASSWORD=$DATABASE_PASSWORD \
  -p DATABASE_HOST=$DATABASE_HOST \
  -p DATABASE_PORT=$DATABASE_PORT \
 | oc create -f -
```
+
. Get the route
+
```
$ oc get route
NAME                     HOST/PORT                                             PATH      SERVICES                 PORT      TERMINATION   WILDCARD
django-psql-persistent   django-psql-persistent-app.apps.kwtest.osadev.cloud             django-psql-persistent   <all>                   None
```
+
. Now that our application is deployed and popular, let's scale it!
+
```
$ oc scale dc django-psql-persistent --replicas=50
deploymentconfig.apps.openshift.io "django-psql-persistent" scaled
```
+ 
. Check on scale operation
+
```
$ oc get pods
django-psql-persistent-1-wsb96   0/1       Pending     0          1m
django-psql-persistent-1-wxdp7   1/1       Running     0          1m
django-psql-persistent-1-x472c   0/1       Pending     0          1m
django-psql-persistent-1-x9vz9   1/1       Running     0          1m
django-psql-persistent-1-xng6x   0/1       Pending     0          1m
django-psql-persistent-1-zntvc   1/1       Running     0          1m
django-psql-persistent-1-zqwmz   0/1       Pending     0          1m
django-psql-persistent-1-ztgbg   0/1       Pending     0          1m
```
+
. View the `describe` on the pods that are in `Pending`
+
```
...
Events:
  Type     Reason            Age                From               Message
  ----     ------            ----               ----               -------
  Warning  FailedScheduling  4m (x167 over 9m)  default-scheduler  0/7 nodes are available: 1 Insufficient memory, 6 node(s) didn't match node selector.
```

== Scale up the cluster

The previous application scale has exhausted our cluster resources.  The 
application has proven to be more popular than our current cluster capacity. To
solve this problem the cluster will need to scale up.

. List the cluster
+
```
az openshift list -g $CLUSTER_NAME
```
+
. Scale up the cluster
+
```
az openshift scale -c 2 -g $CLUSTER_NAME
```
+
. Check on application status and verify all pods are running.
```
$ oc get pods
```

== Deprovision the OpenShift managed cluster

. Remove the cluster
+
```
az openshift delete -g $CLUSTER_NAME
```